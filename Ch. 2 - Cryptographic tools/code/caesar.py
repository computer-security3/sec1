import string
from collections import Counter
import numpy as np
import scipy.stats as stats


def encode_char(char, key):
    return chr((ord(char) - ord("a") + key) % 26 + ord("a"))


def encode_str(string, key):
    return [encode_char(c, key) for c in string]


def encode_file(infile, outfile, key):
    with open(infile) as inf, open(outfile, "w") as outf:
        c = inf.read(1).lower()
        while c:
            if ord("a") <= ord(c) <= ord("z"):  # encode if letter
                outf.write(encode_char(c, key))
            else:  # write as plain otherwise
                outf.write(c)
            c = inf.read(1).lower()


TELEGRAPHIC_ENGLISH_FREQUENCIES = np.array([
    0.0804,
    0.0148,
    0.0334,
    0.0382,
    0.1249,
    0.024,
    0.0187,
    0.0505,
    0.0757,
    0.0016,
    0.0054,
    0.0407,
    0.0251,
    0.0723,
    0.0764,
    0.0214,
    0.0012,
    0.0628,
    0.0651,
    0.0928,
    0.0273,
    0.0105,
    0.0168,
    0.0023,
    0.0166,
    0.0009])

# Normalise the distribution (because decimal notations induce big errors)
TELEGRAPHIC_ENGLISH_FREQUENCIES /= np.sum(TELEGRAPHIC_ENGLISH_FREQUENCIES)


def filter_lower_alpha(string):
    return (char for char in string.lower() if char.isalpha())


class Frequencies(Counter):
    def __init__(self, shift=0):
        super().__init__()
        self._shift = shift

    def update(self, iterable=None, /, **kwargs):
        if iterable is None:
            return
        super().update(encode_str(filter_lower_alpha(iterable), self._shift),
                       **kwargs)

    def chi_square(self):
        counts = np.asarray([self[c] for c in string.ascii_lowercase],
                            dtype=np.float64)
        counts /= np.sum(counts)  # normalise
        return stats.chisquare(counts, TELEGRAPHIC_ENGLISH_FREQUENCIES)


def guess_key(file, nb_chars=None):
    best_key = None
    lowest_chi = float("inf")

    with open(file) as inf:
        chars = inf.read(nb_chars).lower()
        for shift in range(1, 25):  # find the best chi?
            freq = Frequencies(shift=shift)
            freq.update(chars)

            chi_square, _ = freq.chi_square()
            if chi_square < lowest_chi:
                best_key = shift
                lowest_chi = chi_square

    return best_key
