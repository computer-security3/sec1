import caesar

with open("../resource/ciphered.txt") as inf:
    line = inf.readline()
    for key in range(1, 26):
        print("key = ", key)
        brute = " ".join(caesar.encode_str(caesar.filter_lower_alpha(line), key))
        brute = brute.replace(" ", "")
        print(brute, "\n")