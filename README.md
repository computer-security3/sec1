# Introduction to cybersecurity - Romain Absil (rabsil@he2b.be)

The last version of the slides can always be accessed from [https://computer-security3.gitlab.io/sec1](https://computer-security3.gitlab.io/sec1).

The following instructions detail how to build them yourself.

## Requirements

To build slides, you need to have 
- an updated (and complete) LaTeX distribution, such as texlive,
- the make utility tool,
- the latexmk tool,
- inkscape.

Additionaly, to build a preview of the slides, you need xdg-open.

## Build

### Make from within a chapter directory
#### Build while editing
To build and preview the slides with an automatically triggered rebuild of the slides when one of the dependant files changes:
```
make preview
```

#### Build directly
Make the handouts and the slides:
```
make
```

Make the slides:
```
make slides
```

Make the handouts:
```
make handout
```

### Make from top-level directory

#### Build a chapter
Make chapter 4, [everything/handouts/slides/clean]:
```
make [ch4|ch4-handout|ch4-slides|ch4-clean]
```

#### Build everything

Make all the handouts using 4 build processes:
```
make -j4 [handouts|slides]
```

Make *everything*, slides and handout:
```
make -j4
```

#### Clean everything
```
make clean
```
