\input{../common/header.tex}
\input{../common/cmds.tex}

\title{Ch. 3 - Certificates and PKI}

\begin{document}
\input{../common/front.tex}

\section{Introduction}

\begin{frame}
\frametitle{Huston, we have a problem. What is it?}
\begin{itemize}[<+->]
\item Consider the context of encrypting data
\end{itemize}
\begin{exampleblock}<+->{Scenario}
    \begin{itemize}[<+->]
    \item Alice wants to send a message that only Oscar can read
    \item Alice ciphers the message with the public key of Oscar
    \item Alice sends the message to Oscar
    \end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Only the owner of the private key matching the public one that was used can uncipher the message
\end{itemize}
\begin{alertblock}<+->{Problem}
    \begin{itemize}[<+->]
    \item Anyone could have generated a pair of keys
    \item We don't know if the owner is actually Oscar
    \end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}
\frametitle{Nothing. Please, tell me.}
\begin{itemize}[<+->]
\item Consider the context of signing data
\end{itemize}
\begin{exampleblock}<+->{Scenario}
    \begin{itemize}[<+->]
    \item Alice wants to make sure some message comes from Oscar
    \item Oscar signs the message with his private key
    \item Oscar sends the message to Alice
    \end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Only the owner of the private key matching the public one that was used to authenticate could have signed
\end{itemize}
\begin{alertblock}<+->{Problem}
    \begin{itemize}[<+->]
    \item Anyone could have generated a pair of keys
    \item We don't know if the owner is actually Oscar
    \end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}
\frametitle{No. If you cared, you would know what the problem is.}
\begin{itemize}[<+->]
\item In both previous examples, we don't actually know who is Oscar
    \begin{itemize}
    \item Anyone could have generated a pair of keys
    \item Anyone can pretend to be Oscar
    \end{itemize}
\item We need to link public keys to their owners
    \begin{itemize}
    \item Some verification of the actual identify of the owner must be made
    \end{itemize}
\item Because of the scale of the system, centralisation cannot be used
    \begin{itemize}
    \item Some form of consensus must be made
    \end{itemize}
\end{itemize}
\begin{exampleblock}<+->{Basic consensus rules}
    \begin{itemize}[<+->]
    \item The actors of the consensus set up a set of rules that they agree to follow
    \item The actors of the consensus decide who among them can be trusted
    \item The actors of the consensus have to be trusted by outsiders
    \end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}
\frametitle{Man in the middle attack (MITM)}
\begin{overprint}
\begin{itemize}[<+->]
\only<1-5|handout:0>{\item The regular situation}
\only<6-|handout:1>{\item The attack}
\end{itemize}
\end{overprint}
\begin{center}
\scalebox{.8}{
\begin{tikzpicture}
\visible<2->{\node at (0,0) (user) {\includesvg[height=1cm]{pics/user.svg}};}
\visible<3->{\node at (6,0) (server) {\includesvg[width=1cm]{pics/server.svg}};}
\visible<7->{\node at (3,2) (attacker) {\includesvg[width=1cm]{pics/attacker.svg}};}

\visible<4-5|handout:0>{\draw[thick, ->, >=stealth] (user.15) -- (server.165);}
\visible<5|handout:0>{\draw[thick, ->, >=stealth] (server.195) -- (user.345);}
\visible<9->{\draw[ultra thick] (attacker) -- (3,-1);}

\visible<8-9|handout:0>{\draw[thick, ->, >=stealth] (user) -- (2.75,0);}
\visible<10->{\draw[thick, red!80, ->, >=stealth] (user) to[out=0,in=265] (attacker);}
\visible<11->{\draw[thick, red!80, ->, >=stealth] (attacker) to[out=275,in=180] (server);}
\visible<12->{\draw[thick, red!80, ->, >=stealth] (server) to[out=160,in=0] (attacker);}
\visible<13->{\draw[thick, red!80, ->, >=stealth] (attacker) to[out=180,in=20] (user);}
\end{tikzpicture}}
\end{center}
\end{frame}

\section{Certificate and PKI}

\begin{frame}
\frametitle{Principle}
\begin{itemize}[<+->]
\item Alice and Oscar have a common relation that they both trust
\item Oscar meets this trusted third party intermediate and gives his public key
\item The intermediate recognises Oscar and guarantees his identity
\item The intermediate uses his own private key to sign the public key of Oscar, and sends it to Alice
	\begin{itemize}
	\item Alice can check that the private key has not been altered
	\item Alice can check that the key has been signed by the trusted intermediate.
	\end{itemize}
\item Alice can authenticate the message without risks
\item In practice, a third party trusted intermediate is called a \emph{certification authority}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Examples of certification authorities}
\begin{small}
\begin{center}
\begin{tabular}{|l|c|c|}
\hline
\textbf{Nom} & \textbf{Use} & \textbf{Market share}\\
\hline\hline
Identrust & 51.8\% & 56.6\% \\
Sectigo & 11.7\% & 12.8\% \\
GlobalSign & 8.9\% & 9.8\% \\
Digicert & 8.4\% & 9.2\% \\
Let's Encrypt & 5.9\% & 6.4\% \\
GoDaddy & 4.6\% & 5.0\% \\
Certum & 0.6\% & 0.7\% \\
\hline
\end{tabular}
\end{center}
\end{small}
\begin{itemize}
\item Source: \emph{Usage of SSL certificate authorities for websites}, \url{http://w3techs.com/technologies/overview/ssl_certificate/all} - 15/09/2023.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Certificates and PKI}
\begin{exampleblock}<+->{Terminology}
	\begin{itemize}[<+->]
	\item \emph{(Public key) certificate} : electronic document used to prove the property of a public key
	\item \emph{Public key infrastructure (PKI)} : set of hardware, software, people, policies and protocols needed to create, manage, distribute, use, store and revoke certificates
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Goal of a certificate: proves the ownership a public key
\item Goal of a PKI
	\begin{enumerate}
	\item Make information transfer easier in network services (e-trade, etc.)
	\item Link public keys to their owners with a certification authority
	\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The idea}
\begin{itemize}[<+->]
\item Certificates contain a public key, and information about its owner
\item Certificates used for websites detail the domains for which they are valid
\item Certificates are \emph{issued} and \emph{signed} by a CA
    \begin{itemize}
    \item The CA guarantees the identity of the the owner
    \item If you trust the CA, you can trust the owner
    \end{itemize}
\item The signatures tells you that
    \begin{itemize}
    \item the certificate is genuinely issued by the CA
    \item the certificate is integer
    \end{itemize}
\item The X.509 format makes the exchange and use of certificates easier
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{X.509 certificates}
\begin{itemize}[<+->]
\item PKI standard
\item Specifies, among others, the format of public key certificates, their revocation, validation paths, etc.
\item Structure of a X.509 v3 certificate :
	\begin{itemize}
	\item Version number
	\item Serial number
	\item Signature algorithm
	\item Emitter
	\item Validity period
	\item Name of the client
	\item Information related to the public key of the client
		\begin{itemize}
		\item Algorithm
		\item Public key
		\end{itemize}
	\item Additional information
	\item Algorithm used to sign the certificate
	\item Signature of the certificate
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Example of errors}
\begin{itemize}[<+->]
\item Unknown issuer
    \begin{itemize}
    \item The certificate has been issued and signed by somebody you don't trust
    \item The trusted CA are usually either OS- or browser-provided
    \end{itemize}
\item Expired certificate
    \begin{itemize}
    \item The period of validity gives some form of protection against forge (same with currency)
    \item Allows to discard deleted websites
    \item Allows CA to bill their clients regularly
    \end{itemize}
\item Invalid signature
    \begin{itemize}
    \item Probably means forgery
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Self-signed certificates}
\begin{itemize}[<+->]
\item Certificated issued by the owner of the embedded public key
    \begin{itemize}
    \item No certification authority issued them
    \end{itemize}
\end{itemize}
\begin{alertblock}<+->{Problem}
    \begin{itemize}[<+->]
    \item Who are you?
    \item The King of England
    \item Who guarantees it?
    \item Me, the King of England
    \end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item If a website delivers a self-signed certificate, we cannot check wether an attacker replaced it with his own
    \begin{itemize}
    \item Acceptable during development, and for root certificates (cf. Chain of Trust)
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Certificate signing request}
\begin{itemize}[<+->]
\item Most of the times, a CA does not fully generate a certificate
    \begin{itemize}
    \item If they did, they generated a keypair, and consequently have a copy of the private key
    \end{itemize}
\item The keypair is generated on client side
\item The public key is embedded in a partial certificate, to be signed by the CA
    \begin{itemize}
    \item Certificate signing request (CSR)
    \end{itemize}
\item The CSR contains identification information, the public key, information about the cipher, and is signed
\item It is send to the CA, who
    \begin{enumerate}
    \item checks the signature (proves integrity, and that you have the private key)
    \item signs it (further information can be required and identity checks can be made)
    \end{enumerate}
\item You then have a full certificate
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Revoking certificates}
\begin{itemize}[<+->]
\item Certificates can be revoked before their expiration date
    \begin{itemize}
    \item After misuse, or after the private key is compromised
    \end{itemize}
\item Two main techniques : CRLs and OCSP
\end{itemize}
\begin{exampleblock}<+->{Certificate revocation list (CRL)}
    \begin{itemize}[<+->]
    \item The browser periodically queries trusted CAs to know which certificates have been revoked
    \end{itemize}
\end{exampleblock}
\begin{exampleblock}<+->{Online certificate status protocol}
    \begin{itemize}[<+->]
    \item The browser crafts a control message for a specific certificate and queries an OCSP server
    \end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Illustration with OpenSSL}
\begin{enumerate}
\item Create a keypair
\begin{lstlisting}
openssl genpkey -algorithm RSA -out private_key.pem
openssl rsa -pubout -in private_key.pem -out public_key.pem
\end{lstlisting}
\item Prepare a file (e.g., \texttt{cert.cnf}) for identification information,...
    \begin{itemize}
    \item Can be done interactively with the prompt
    \end{itemize}
\item Create CSR
\begin{lstlisting}
openssl req -new -key private_key.pem -out certificate.csr -config cert.cnf
\end{lstlisting}
\item Use the CSR to create a self-signed certificate
    \begin{itemize}
    \item Or send the CSR to your CA
    \end{itemize}
\begin{lstlisting}
openssl x509 -req -days 365 -in certificate.csr -signkey private_key.pem
    -out certificate.crt -extensions v3_req -extfile cert.cnf
\end{lstlisting}
\item Viewing the certificate
\begin{lstlisting}
openssl x509 -in certificate.crt -out certificate.pem
openssl x509 -in certificate.pem -text -noout
\end{lstlisting}
\end{enumerate}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{OpenSSL identification information file}
\begin{lstlisting}

[req]
default_bits = 2048
distinguished_name = req_distinguished_name
x509_extensions = v3_req
prompt = no

[req_distinguished_name]
C = BE
ST = Brussels-Capital
L = Brussels
O = MSW
CN = My super website

[v3_req]
keyUsage = keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[alt_names]
DNS.1 = mywebsite.be
\end{lstlisting}
\end{frame}

\section{Chain of trust}

\begin{frame}
\frametitle{Who can we trust?}
\begin{exampleblock}<+->{How can we trust a CA?}
    \begin{itemize}[<+->]
    \item Consensus
    \end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item A consensus of CA decides which CA to trust, and which cannot
\item ``Large'' software companies also decide which CA can be trusted
    \begin{itemize}[<+->]
    \item Microsoft, Apple, Linux foundation, Mozilla, Google, etc.
    \item Companies that provide browsers or secure authentication
    \end{itemize}
\item Good faith prevails
    \begin{itemize}
    \item If a CA misbehaves, others CA disavow it
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Examples}
\begin{itemize}[<+->]
\item Security breach
    \begin{itemize}
    \item The CA is hacked, and issues fraudulent certificates
    \item Allows hackers to use fraudulent certificates to MitM domains
    \item Historical example: DigiNotar in 2011
    \end{itemize}
\item Deliberate behaviour
    \begin{itemize}
    \item If a governement controls a CA, it can force it to issue fraudulent certificates
    \item Allows a governement to MitM domains
    \item Historical example: China Internet Network Information Center in 2009 and 2015
    \end{itemize}
\item Basic defense: takeover, dismantle, untrust such CA
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Chain of trust (CoT)}
\begin{itemize}[<+->]
\item If is difficult, on practice, to have only a few CA
    \begin{itemize}
    \item Lots of different CA, with different price ranges, different requirements, etc.
    \end{itemize}
\end{itemize}
\begin{exampleblock}<+->{Question}
    \begin{itemize}[<+->]
    \item How can a certificate issued by some CA recognised by Apple can be trusted on somebody running a Microsoft software?
    \end{itemize}
\end{exampleblock}
\begin{block}<+->{Answer}
    \begin{itemize}[<+->]
    \item Apple and Microsoft and a third party intermediate that they both trust
    \end{itemize}
\end{block}
\begin{itemize}[<+->]
\item That CA signs both Microsoft and Apple certificates
\item Such ``high level'' CAs are called \emph{root} CAs
    \begin{itemize}
    \item Web browsers pack up lists of root CAs they trust
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Root certificate}
\begin{itemize}[<+->]
\item CA issue certificates in the form a a tree structure
\item The highest certificate in a CoT is called a root certificate
    \begin{itemize}
    \item A root certificate is a certificate identifying a root CA
    \end{itemize}
\item By definition, root certificates are self-signed
    \begin{itemize}
    \item In practice, they have multiple multiple trust-paths, e.g., in cases of cross-signatures
    \item They can also be checked by some form of secure physical distribution
    \end{itemize}
\item The HTTPS PKIs depend on a set of root certificates
    \begin{itemize}
    \item If some certificate is presented, we check if it is signed by a root CA
    \item If not, we check if the signatory has a certificate signed by a root CA
    \item Etc.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Illustration}
\begin{center}
\scalebox{.6}{
\begin{tikzpicture}
\onslide<3->{\draw[rounded corners=5pt, thick] (0,0) rectangle (5,1.5);}
\onslide<3->{
\foreach \i in {1,2,...,5}
    \node[draw, minimum height=0.5cm, minimum width=0.5cm, inner sep=0pt, label={above:\footnotesize RCA\i}] (CA\i) at (\i-0.5, 0.5) {};
\node[anchor=west] at (0,2) {\textbf{Trusted Root CAs}};
}

\onslide<1->{\node (user) at (-1,-8) {\includesvg[width=0.75cm]{pics/user.svg}};}
\onslide<2->{\draw[thick, ->, >=stealth] (user) -- node[pos=.5, above, sloped] {trusts} (-1,0.75) -- (-0.1,0.75);}

\onslide<5->{\node (web) at (16,-8) {\textcolor{blue!80}{\texttt{www.mywebsite.com}}};}
\onslide<4->{\draw[thick, ->, >=stealth] (user) -- (web) node[midway, below] {connects};}
\onslide<7->{\node[draw] (webcert) at (16,-6) {Certificate : \texttt{mywebsite.com}};}
\onslide<6->{\draw[->, >=stealth, Green] (web) -- (webcert) node[midway, right] {has certificate};}

\onslide<9->{\node[draw] (CA12) at (10,-6) {CA12};}
\onslide<11->{\node[draw] (CA12cert) at (10,-4) {Certificate : CA12};}
\onslide<10->{\draw[->, >=stealth, Green] (CA12) -- (CA12cert) node[midway, right] {has certificate};}
\onslide<8->{\draw[->, >=stealth, Green] (webcert) -- (CA12) node[midway, above] {references};}

\onslide<13->{\node[draw] (CA27) at (5,-4) {CA27};}
\onslide<15->{\node[draw] (CA27cert) at (5,-2) {Certificate : CA27};}
\onslide<12->{\draw[->, >=stealth, Green] (CA12cert) -- (CA27) node[midway, above] {references};}
\onslide<14->{\draw[->, >=stealth, Green] (CA27) -- (CA27cert) node[midway, right] {has certificate};}

\onslide<16->{\draw[->, >=stealth, Green] (CA27cert) -- (CA2) node[midway, right, xshift=5pt] {references};}

\onslide<17->{\draw[->, red!80] (CA2) |- (CA27cert) node[pos=.5, left] {signs};}
\onslide<18->{\draw[->, red!80] (CA27) to[out=330,in=210] node[pos=.5, below] {signs} (CA12cert);}
\onslide<19->{\draw[->, red!80] (CA12) to[out=330,in=210] node[pos=.5, below] {signs} (webcert);}
\end{tikzpicture}}
\end{center}
\end{frame}

\end{document}
